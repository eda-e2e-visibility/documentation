# End-to-end visibility of event-driven

In this prototype I am proposing a solution to one of the challenges of building an event-driven choreographed services around event-streaming platform.

## Introduction

In choreographed services there is no central orchestrator that manage the end-to-end business process. Communication between those services happen through events.
For more details about choreographed services vs standard orchestration please check [Microservices — When to React Vs. Orchestrate](https://medium.com/capital-one-tech/microservices-when-to-react-vs-orchestrate-c6b18308a14c)
Also (Advantages of Choreography vs Orchestration)[https://solace.com/blog/microservices-choreography-vs-orchestration/] The following diagram shows a high level view of our order management system and involved services and events.

<p align="center">
  <img  src="resources/order-choreographed-services.png?raw=true" title="Choreographed services of Order management">
</p>  
 
## Challenges of event-driven

When adopting event-driven architecture with event-streaming platform you might need to think about some areas like the following 

### Distributed tracing

Unlike orchestration communication which is based on remote procedure point-to-point calls (RPCs), 
involves services communicating directly with each other and has explicit dependencies.
Choreographed services contain implicit dependencies and rely on how the components handle and emit events.
One of the challenges with distributed choreographed services is tracing the flow in all involved services.
So we can debug specific order using its ID to resolve issues for example.
The following blog post descibes in details how we can enable Zipkin in Kafka-based services [Distributed tracing with Zipkin](https://www.confluent.io/blog/importance-of-distributed-tracing-for-apache-kafka-based-applications/)


### Business process visibility

Distributed tracing could be seen as a passive tool, it could be used to know the flow of order for example. 
It could help us answering some questions like

* What are the steps that specific order went through? Which services were involved in processing this order.
* How long it takes in each service of the workflow to process orders.

This is great for debugging, but it still does not cover some business needs that require real-time tracking and actions like the following

#### Track SLA of each workflow steps

Track SLA of each service in the workflow. So if business entity (in our case it is Order) is stuck in this service for long time, 
we might expire it and acknowledge other services in the workflow to stop any further processing. Lets take an example from our prototype.
If `order-placed` is consumed by payment-service and for some reason the service is taking too long to process the order.
We might need to do some actions like for example

* Cancel the order completely and compensate all previous completed steps.
* Send and email to the customer asking if he is still interested in this order and can wait for it to finish.

Each service in the workflow needs to support this feature and take an action when it detects this behaviour.

#### Track SLA of the end-to-end business process

Lets imagine that the business have a commitment to their customers to process their orders in 30 seconds.
This means that we need to track the orders when they arrive in our workflow all the way to the end.
In our example the time between `order-placed` and `order-completed` should be less that 30 seconds.
If orders are taking long time because they are stuck in any workflow step or there are some issues in some Kafka topics, we might need to expire this order and ask all
workflow steps which have not done their work yet to ignore those orders. While other services that already processed this order should run in compensation mode to roll back what they did.
To support this feature we need some tracking tool that is part of the workflow like any other service. 

#### Accurately draw a workflow orders went through

Given that there is no central orchestrator which knows the workflow that orders should go through, and we only have events flowing around between services, we might need to draw the flow of steps that orders went through in real-time.
This needs to support different types of orders which require different workflow steps. It also should be always up-to-date with the actual production code.
This ordering of the involved workflow steps need to be accurate and does not depend only on event time. 
Because there might be time drifting between services involved in the workflow.
So we need some kind of strong ordering guarantee of the steps that orders actually went through.

## Event-driven tracing & real-time visibility

From the challenges and business requirements mentioned above, there might be a need find a solution that supports real-time tracking of
workflow of choreographed services. One of the solutions we thought about is using events themselves to do this job. We called this type `Operational Events`.

### Operational events intro
 
If each service in the workflow tells what they have done related to our bounded context entity(in our prototype this is Order) and moves this entity between workflow steps till the end. 
For those operational events to be most effective the following key design requirements need to be taken care of
* Publishing operational event should be published in the same Kafka transaction as result events that services usually publish.
* Operational events are published to the same Kafka topic, and because they are all related to the same bounded context, they should be using the same key(OrderId in our prototype). 
Using the same key here means strong ordering guarantee naturally supported by Kafka.
* Operational event should contain the current step of the workflow and may be the previous step if exists.

<p align="center">
  <img  src="resources/operational_event.png?raw=true" title="Operational events" height="500">
</p>

### Operational events implementation

The following code snippets show how operational events are published using Kafka producer(using spring-kafka) and Kafka Streams (using spring-cloud-streams).

#### Kafka producer publishing

As shown in this code snippet, we publish both business event `order-placed` along side the `workflow-step-completed` event in the same Kafka transaction.

<p align="center">
  <img  src="resources/OperationalEvent_KafkaTemplate.png?raw=true" title="Operational events" height="250">
</p>

#### Kafka streams publishing

With Kafka streams, it is easier to publish operational events along side the business event as shown in the following code example.

<p align="center">
  <img  src="resources/OperationalEvent_Streams.png?raw=true" title="Operational events" height="250">
</p>